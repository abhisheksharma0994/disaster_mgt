#import sys

#sys.path.append('/usr/local/lib/python3.4/dist-packages')
import requests
import os
import glob
import time
from gps import *
from time import *
import numpy as np
import cv2

gpsd = None #seting the global variable
latitude = 0
longitude = 0

gpsd = gps(mode=WATCH_ENABLE)

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

url = 'http://52.23.167.124:5000/upload'

isDoing=False;

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c, temp_f

def GetGPSData():
    while (int(latitude)==0 and int(longitude)==0):
        latitude = gpsd.fix.latitude
        longitude = gpsd.fix.longitude
        gpsd.next()
    lat=latitude
    lon=longitude
    #Reset the Global Lat, Long for next Time
    latitude = 0
    longitude = 0 
    return lat,lon

def TakePicture(lat,lon):
    print('taking pic')
    camera = cv2.VideoCapture(0)
    retval, im = camera.read()
    print(retval)
    print(im)
    file = str(lat)+'_'+str(lon)+'_.jpg'
    cv2.imwrite(file, im)
    del(camera)
    return file

def UploadPhoto(file):
    files={'file':open(file,'rb')}
    r=requests.post(url,files=files)
    print(r.text)

tc,tf = read_temp()
threshold = tc + 1;
print('Threshold: '+str(threshold))

while True:
    tc,tf = read_temp()
    print(tc,tf)
    if tc > threshold:
        if isDoing!=True:
            isDoing=True;
            lat,lon=GetGPSData()
            file=TakePicture(lat,lon)
            UploadPhoto(file)
        elif isDoing==True:
            isDoing=False;
        time.sleep(1)
