function add_marker(){
  var mapDiv = document.getElementById('map');
  var map = new google.maps.Map(mapDiv, {
  center: {lat: 21.000, lng: 78.000},
  zoom: 2
});
  $.each(mapPlot, function(key, val) {
    var myLatlng = new google.maps.LatLng(val.lat,val.long);
    var img=location.host+"/uploads/"+val.file;
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Lattitude: ' + val.lat + ' Longitude: ' + val.long + ' Count: '+val.count
     
    });
     
    var infowindow = new google.maps.InfoWindow({
    content: '<IMG BORDER="0" ALIGN="Left" WIDTH="100px" SRC="http://'+img+'">'
  });

    
    marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

    marker.setMap(map);
  });
}

$(document).ready(function() {

  // var results = '<a href="" class="list-group-item active disabled">Recent People</a>';
  var results='<button type="button" class="btn  btn-primary active btn-block">Recent People</button><div class="list">';
  $.each(peopleRescued, function(key, val) {
    var item='<div><a href="#" data-html="true" data-toggle="tooltip" data-placement="bottom" class="list-group-item text-center person-name" title="'+'Contact='+val.contact+'<br>Gender='+val.gender+'<br>Age='+val.age+'<br>POC_Name='+val.pocname+'<br>POC_mob='+val.pocmobile+'">';
    item += val.firstname +' '+val.lastname + '</a></div>';
    results+=item;
  });
  results+='</div>';
  $("#recent_people").html(results);
  $('[data-toggle="tooltip"]').tooltip();
  var search_result;
  $('#search_but').on('click', function() {
    var text_value = $("#search_text").val();
  });
  var options = {
  valueNames: [ 'person-name' ]
};

var userList = new List('people', options);

});
