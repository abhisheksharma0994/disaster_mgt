from flask import Flask, render_template,jsonify, request, json, redirect,send_from_directory
from flask.ext.pymongo import PyMongo
from bson import json_util
import os, subprocess

app = Flask(__name__,static_url_path='')

UPLOAD_FOLDER = 'uploads/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

mongo = PyMongo(app)

@app.route('/')
def home():
    people_rescuedCursorObject = mongo.db.people_rescued.find({})
    map_plotCursorObject = mongo.db.map_plot.find({})
    people_rescued = []
    map_plot = []
    for i in people_rescuedCursorObject:
        people_rescued.append(i)
    for i in map_plotCursorObject:
        map_plot.append(i)
    return render_template('home.html', people_rescued=json_util.dumps(people_rescued), map_plot=json_util.dumps(map_plot))

@app.route('/upload',methods=['POST','GET'])
def upload():
    f=request.files['file']
    print(f.filename)
    item=f.filename.split('_',2)
    if f:
          f.save(os.path.join(app.config['UPLOAD_FOLDER'], f.filename.encode('ascii','ignore')))
    cmd='python thermal_human.py uploads/'+f.filename.encode('ascii','ignore')
    p=subprocess.Popen(cmd,stdout=subprocess.PIPE,shell=True)
    out, err=p.communicate()
    result=out.split('\n')
    print(result)
    if int(result[0]) > 0:
      mongo.db.map_plot.insert_one({'file': f.filename,'lat':item[0].encode('ascii','ignore'),'long':item[1].encode('ascii','ignore'), 'count':result[0]})
    return 'Uploaded Successfully at:'+'/upload/'+f.filename

@app.route('/uploads/<path:path>')
def uploads(path):
    print (path+"   lelo")
    return send_from_directory('uploads', path)


@app.route('/form_new_entry',methods=['POST'])
def form_new_entry():
    if request.method=='POST':
        aadhar=str(request.form['inputaadhar'])
        gender=request.form['inputgender']
        firstname=request.form['inputfirstname']
        lastname=request.form['inputlastname']
        age=request.form['inputage']
        contact=request.form['inputmobile']
        pocname=request.form['inputpocname']
        pocmobile=request.form['inputpocnumber']
        #img=request.form['username']
        print(request.form)
        mongo.db.people_rescued.insert_one({'aadhar':aadhar,'gender':gender,'firstname':firstname,'lastname':lastname  ,'age':age  ,'contact':contact  ,'pocname':pocname  ,'pocmobile':pocmobile  })
    return redirect('/', 301)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
