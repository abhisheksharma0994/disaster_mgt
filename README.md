#Installation

#Run
```python
pip install -r requirements.txt
```

#Install MongoDB (Ubuntu 14.04)
DOCS https://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/

#Run
```python
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo service mongod start
```

#Insert Data into DB

#Run
```python
mongo
use routes
db.people.insert([{"aadhar":1,"gender":"Male","first_name":"Stephen","last_name":"Mitchell","age":94,"contact":"86-(660)806-1793","lat":"46.52575","long":"126.90525","family":"Topicshots","poc":"Stephen Mitchell","poc_contact":"351-(507)340-2006","img":"http://dummyimage.com/201x154.png/5fa2dd/ffffff"},
{"aadhar":2,"gender":"Female","first_name":"Ruby","last_name":"Powell","age":69,"contact":"976-(353)766-1012","lat":"47.74167","long":"96.84444","family":"Meembee","poc":"Ruby Powell","poc_contact":"351-(766)526-7779","img":"http://dummyimage.com/250x228.png/dddddd/000000"},
{"aadhar":3,"gender":"Male","first_name":"Donald","last_name":"Fowler","age":25,"contact":"66-(311)138-6497","lat":"15.85851","long":"104.62883","family":"Realbuzz","poc":"Donald Fowler","poc_contact":"30-(802)983-6631","img":"http://dummyimage.com/174x173.jpg/cc0000/ffffff"},
{"aadhar":4,"gender":"Female","first_name":"Rebecca","last_name":"Moore","age":18,"contact":"380-(547)367-6377","lat":"47.71758","long":"35.52882","family":"Edgeclub","poc":"Rebecca Moore","poc_contact":"389-(611)501-2572","img":"http://dummyimage.com/130x148.jpg/5fa2dd/ffffff"},
{"aadhar":5,"gender":"Male","first_name":"Lawrence","last_name":"Matthews","age":129,"contact":"598-(175)111-3386","lat":"-33.5165","long":"-56.89957","family":"Buzzdog","poc":"Lawrence Matthews","poc_contact":"256-(835)170-7302","img":"http://dummyimage.com/117x138.gif/dddddd/000000"},
{"aadhar":6,"gender":"Male","first_name":"Samuel","last_name":"Reed","age":105,"contact":"967-(103)705-4356","lat":"16.16406","long":"44.77692","family":"Katz","poc":"Samuel Reed","poc_contact":"351-(423)458-0332","img":"http://dummyimage.com/108x223.gif/dddddd/000000"},
{"aadhar":7,"gender":"Female","first_name":"Judy","last_name":"Franklin","age":6,"contact":"62-(128)732-1824","lat":"-8.1014","long":"112.5036","family":"Fanoodle","poc":"Judy Franklin","poc_contact":"7-(434)913-5252","img":"http://dummyimage.com/135x129.png/cc0000/ffffff"},
{"aadhar":8,"gender":"Male","first_name":"Willie","last_name":"Romero","age":114,"contact":"420-(624)967-8334","lat":"49.52721","long":"17.58594","family":"Roombo","poc":"Willie Romero","poc_contact":"84-(155)776-5274","img":"http://dummyimage.com/232x236.jpg/dddddd/000000"},
{"aadhar":9,"gender":"Male","first_name":"Sean","last_name":"Wheeler","age":106,"contact":"86-(203)423-9183","lat":"26.11381","long":"118.8131","family":"Brainverse","poc":"Sean Wheeler","poc_contact":"1-(387)821-5726","img":"http://dummyimage.com/162x174.jpg/5fa2dd/ffffff"},
{"aadhar":10,"gender":"Female","first_name":"Donna","last_name":"Diaz","age":145,"contact":"380-(817)455-7711","lat":"49.83986","long":"35.30442","family":"Jamia","poc":"Donna Diaz","poc_contact":"7-(648)745-6874","img":"http://dummyimage.com/127x235.gif/ff4444/ffffff"},
{"aadhar":11,"gender":"Male","first_name":"Antonio","last_name":"Richardson","age":51,"contact":"62-(486)739-1627","lat":"-7.2028","long":"108.2022","family":"Yodel","poc":"Antonio Richardson","poc_contact":"62-(609)248-7174","img":"http://dummyimage.com/146x208.jpg/cc0000/ffffff"},
{"aadhar":12,"gender":"Male","first_name":"Mark","last_name":"Tucker","age":15,"contact":"593-(445)122-7846","lat":"-2.89264","long":"-78.77814","family":"Thoughtworks","poc":"Mark Tucker","poc_contact":"31-(687)477-9295","img":"http://dummyimage.com/112x116.png/ff4444/ffffff"},
{"aadhar":13,"gender":"Male","first_name":"Carl","last_name":"Freeman","age":128,"contact":"27-(976)707-9916","lat":"-25.63473","long":"27.78022","family":"Yambee","poc":"Carl Freeman","poc_contact":"30-(361)963-1170","img":"http://dummyimage.com/175x177.png/5fa2dd/ffffff"},
{"aadhar":14,"gender":"Female","first_name":"Kathleen","last_name":"Reynolds","age":36,"contact":"251-(818)271-7681","lat":"4.96667","long":"36.48333","family":"Fivebridge","poc":"Kathleen Reynolds","poc_contact":"63-(898)940-9707","img":"http://dummyimage.com/143x229.jpg/cc0000/ffffff"},
{"aadhar":15,"gender":"Female","first_name":"Anna","last_name":"Howell","age":19,"contact":"1-(303)720-6993","lat":"39.7467","long":"-104.8384","family":"Ooba","poc":"Anna Howell","poc_contact":"84-(245)602-9230","img":"http://dummyimage.com/116x236.gif/5fa2dd/ffffff"},
{"aadhar":16,"gender":"Male","first_name":"Earl","last_name":"Crawford","age":148,"contact":"7-(156)121-2149","lat":"59.99333","long":"30.36389","family":"Yata","poc":"Earl Crawford","poc_contact":"62-(378)765-5769","img":"http://dummyimage.com/157x142.gif/dddddd/000000"},
{"aadhar":17,"gender":"Female","first_name":"Janet","last_name":"Rose","age":115,"contact":"7-(830)416-4040","lat":"59.33222","long":"66.02139","family":"Abata","poc":"Janet Rose","poc_contact":"63-(150)795-1415","img":"http://dummyimage.com/201x167.gif/cc0000/ffffff"},
{"aadhar":18,"gender":"Female","first_name":"Rose","last_name":"Brooks","age":119,"contact":"39-(579)575-3122","lat":"44.6355","long":"10.1074","family":"Jabbersphere","poc":"Rose Brooks","poc_contact":"48-(775)980-7945","img":"http://dummyimage.com/210x226.gif/ff4444/ffffff"},
{"aadhar":19,"gender":"Female","first_name":"Michelle","last_name":"Gardner","age":131,"contact":"86-(775)856-0484","lat":"30.85021","long":"120.91138","family":"Abatz","poc":"Michelle Gardner","poc_contact":"33-(647)246-9547","img":"http://dummyimage.com/173x226.gif/cc0000/ffffff"},
{"aadhar":20,"gender":"Male","first_name":"Victor","last_name":"Andrews","age":92,"contact":"57-(305)877-1209","lat":"5.29622","long":"-75.8839","family":"Feednation","poc":"Victor Andrews","poc_contact":"351-(973)967-2023","img":"http://dummyimage.com/234x217.gif/cc0000/ffffff"}]
)

exit
```

#Running the Project
```python
cd disaster_mgt
python routes.py
```
